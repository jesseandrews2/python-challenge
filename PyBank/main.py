
#Reading CSV file
import os

import csv

#print headers
print("Financial Analysis")
print("----------------------------")

budget_data = os.path.join('..', 'Resources', 'budget_data.csv')

with open(budget_data) as csvfile:

	# CSV reader specifies delimiter and variable that holds contents
	csvreader = csv.reader(csvfile, delimiter=',')
	date=[]
	totalprofit=[]
	profit_change=[]
	#print(csvreader)

	# Read the header row first (skip this step if there is no header)
	csv_header = next(csvreader)
	
	# Read each row of data after the header
	for row in csvreader:
		date.append(row[0])
		totalprofit.append(int(row[1]))
		
		#total months
	##	row_count=len(list(csvfile))
	##	print("Total months: " + str(row_count))

		#print total months
	##	print(f'{sum(totalprofit)}')

	for i in range(len(totalprofit) -1):
		profit_change.append(totalprofit [i +1] - totalprofit[i])
#print results
print(f"Total Months; {len(date)}")
print(f"Total: ${sum(totalprofit)}")
print(f"Average Change: {round(sum(profit_change)/len(profit_change),2)}")
#print Min/Max values/indexing need to clarity
max_roc = max(profit_change)
min_roc = min(profit_change)

max_month = profit_change.index(max_roc) + 1
min_month = profit_change.index(min_roc) + 1



print(f"Greatest increase in profits: {date[max_month]} (${(str(max_roc))})")
print(f"Greatest decrease in profits: {date[min_month]} (${(str(min_roc))})")

#create new CSV
financial_analysis = os.path.join('..', 'Analysis', 'financial_analysis.csv')
with open(financial_analysis,"w") as file:
	#print/write data
	file.write("Financial Analysis")
	file.write("\n")
	file.write("----------------------------")
	file.write("\n")
	file.write(f"Total Months; {len(date)}")
	file.write("\n")
	file.write(f"Total: ${sum(totalprofit)}")
	file.write("\n")
	file.write(f"Average Change: {round(sum(profit_change)/len(profit_change),2)}")
	file.write("\n")
	file.write(f"Greatest increase in profits: {date[max_month]} (${(str(max_roc))})")
	file.write("\n")
	file.write(f"Greatest decrease in profits: {date[min_month]} (${(str(min_roc))})")
