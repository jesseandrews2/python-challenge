#Reading CSV file
import os

import csv

#print headers
print("Election Results")
print("----------------------------")

election_data = os.path.join('..', 'Resources', 'election_data.csv')
total_votes = 0
casper_votes = 0
degette_votes = 0
doane_votes = 0

with open(election_data) as csvfile:

    # CSV reader specifies delimiter and variable that holds contents
    csvreader = csv.reader(csvfile, delimiter=',')
    # Read the header row first (skip this step if there is no header)
    csv_header = next(csvreader)
    # Read each row of data after the header
    #count total votes
    for row in csvreader:
        total_votes += 1

        #if el statements to count votes per person
        if row[2] == "Charles Casper Stockham":
            casper_votes+=1
        elif row[2] == "Diana DeGette":
            degette_votes+=1
        elif row[2] == "Raymon Anthony Doane":
            doane_votes+=1
#create dict. for votes
candidate=["Charles Casper Stockham","Diana DeGette","Raymon Anthony Doane"]
votes=[casper_votes, degette_votes, doane_votes]

candidate_and_votes=dict(zip(candidate,votes))
key=max(candidate_and_votes, key=candidate_and_votes.get)

#summary
casper_percent=(casper_votes/total_votes)*100
degette_percent=(degette_votes/total_votes)*100
doane_percent=(doane_votes/total_votes)*100

#print results

print(f"Total Votes: {total_votes}")
print("----------------------------")
print(f"Charles Casper Stockham: {casper_percent:.3f}, % ({casper_votes})")
print(f"Diana DeGette: {degette_percent:.3f} % ({degette_votes})")
print(f"Raymon Anthony Doane: {doane_percent:.3f} % ({doane_votes})")
print("----------------------------")
print(f"Winner: {key}")
print("----------------------------")

#create new CSV
voting_analysis = os.path.join('..', 'Analysis', 'voting_analysis.csv')
with open(voting_analysis,"w") as file:
	#print/write data
    file.write("Election Results")
    file.write("\n")
    file.write("----------------------------")
    file.write("\n")
    file.write(f"Total Votes: {total_votes}")
    file.write("\n")
    file.write("----------------------------")
    file.write("\n")
    file.write(f"Charles Casper Stockham: {casper_percent:.3f}, % ({casper_votes})")
    file.write("\n")
    file.write(f"Diana DeGette: {degette_percent:.3f} % ({degette_votes})")
    file.write("\n")
    file.write(f"Raymon Anthony Doane: {doane_percent:.3f} % ({doane_votes})")
    file.write("\n")
    file.write("----------------------------")
    file.write("\n")
    file.write(f"Winner: {key}")
    file.write("\n")
    file.write("----------------------------")
    file.write("\n")